@extends('layouts.app')

@section('title',"Edit Transaksi Zakat")
@section('content')
<div class="block-header">
  <h2>BAYAR ZAKAT</h2>
</div>
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>
          MASUKKAN DATA
        </h2>
      </div>
      <div class="body">
        <form class="form-vertical" id="myform" action="{{route('zakat.update', $transaksi)}}" method="POST">
          <h2 class="card-inside-title">DATA MUZAKKI</h2>
          @csrf
          {{ method_field('PATCH') }}
          <input type="hidden" name="_idm" value="{{base64_encode($transaksi->muzakki->id)}}">
          <div class="form-group form-float">
            <div class="form-line">
              <input type="text" name="nama" id="nm" class="form-control" required="" value="{{$transaksi->muzakki->name}}">
              <label class="form-label">Nama Muzakki</label>
            </div>
          </div>

          <div class="form-group form-float">
            <div class="form-line">
              <select name="rt" id="rt" required="" class="select">
                <!-- <option value="">-- pilih rt --</option> -->
                <option value="1" @if($transaksi->muzakki->rt == 1) selected @endif>01</option>
                <option value="2" @if($transaksi->muzakki->rt == 2) selected @endif>02</option>
                <option value="3" @if($transaksi->muzakki->rt == 3) selected @endif>03</option>
                <option value="4" @if($transaksi->muzakki->rt == 4) selected @endif>04</option>
                <option value="5" @if($transaksi->muzakki->rt == 5) selected @endif>05</option>
                <option value="6" @if($transaksi->muzakki->rt == 6) selected @endif>06</option>
              </select>
            </div>
          </div>

          <div class="form-group form-float">
            <div class="form-line">
              <input type="email" id="tp" name="email" class="form-control" value="{{$transaksi->muzakki->email}}">
              <label class="form-label">E-Mail</label>
            </div>
          </div>
          <div class="form-group form-float">
            <div class="form-line">
              <textarea name="alamat" id="al" rows="4" cols="30" class="form-control no-resize" required="">{{$transaksi->muzakki->alamat}}</textarea>
              <label class="form-label">Alamat</label>
            </div>
          </div>
          <div class="form-group form-float">
            <div class="form-line">
              <input type="text" id="tp" name="noHP" class="form-control only-num" maxlength="16" value="{{$transaksi->muzakki->nohp}}">
              <label class="form-label">Nomor Handphone</label>
            </div>
          </div>
          <div class="form-group form-float">
            <input type="radio" name="kelamin" id="laki-laki" class="with-gap" value="L" required="" @if($transaksi->muzakki->jeniskelamin=="L")
            checked
            @endif
            >
            <label class="form-label" for="laki-laki">Laki-laki</label>

            <input type="radio" name="kelamin" id="perempuan" class="with-gap" value="P" @if($transaksi->muzakki->jeniskelamin=="P")
            checked
            @endif
            >
            <label class="form-label" for="perempuan">Perempuan</label>
          </div>
          <h2 class="card-inside-title">DATA ZAKAT</h2>
          <div class="form-group form-float">
            <div>
              <select name="tipe" id="tipe" required="" class="select">
                <option value="">-- pilih jenis zakat --</option>
                <option value="1" @if($transaksi->jeniszakat_id == 1)
                  selected
                  @endif>Zakat Beras</option>
                <option value="2" @if($transaksi->jeniszakat_id == 2)
                  selected
                  @endif>Zakat Uang</option>
                <option value="3" @if($transaksi->jeniszakat_id == 3)
                  selected
                  @endif>Zakat Maal</option>                  
                {{-- @foreach($jenis_zakats as $jenis)
                <option value="{{ $jenis->id }}" @if($transaksi->jeniszakat_id == $jenis->id)
                  selected
                  @endif
                  >{{ $jenis->jenis }}</option>
                @endforeach --}}
              </select>
            </div>
          </div>
          <div class="form-group form-float">

            <input type="radio" name="carabayar" id="none" class="with-gap" value="none" required="" @if($transaksi->carabayar=="none") checked @endif>
            <label class="form-label" for="none">None</label>

            <input type="radio" name="carabayar" id="cash" class="with-gap" value="cash" required="" @if($transaksi->carabayar=="cash") checked @endif>
            <label class="form-label" for="cash">Cash</label>

            <input type="radio" name="carabayar" id="transfer" class="with-gap" value="transfer" @if($transaksi->carabayar=="transfer") checked @endif>
            <label class="form-label" for="transfer">Transfer</label>
          </div>          
          <div class="input-group">
            <span class       = "input-group-addon">Jumlah Muzakki</span>
            <div class="form-line">
              <input type="text" required="" name="jiwa" id="jiwa" class="form-control only-num" placeholder="Jumlah Jiwa" value="{{$transaksi->jiwa}}">
            </div>
            <span class="input-group-addon">Jiwa</span>
          </div>
          <div class="input-group">
            <span class       = "input-group-addon">Zakat Beras</span>
            <div class="form-line">
              <input type="text" id="beras" name="beras" class="form-control dec-num" placeholder="Jumlah Beras" value="{{$transaksi->beras_fitrah}}">
            </div>
            <span class="input-group-addon">Kg</span>
          </div>
          <div class="input-group">
            <span class="input-group-addon">Rp</span>
            <div class="form-line">
              <input type="text" name="uang" id="uang" class="form-control only-num" placeholder="Jumlah Zakat Fitrah Uang" value="{{$transaksi->uang_fitrah}}">
            </div>
          </div>
          <div class="input-group">
            <span class="input-group-addon">Rp</span>
            <div class="form-line">
              <input type="text" name="fidyah" class="form-control only-num" placeholder="Jumlah Fidyah" value="{{$transaksi->fidyah}}">
            </div>
          </div>
          <div class="input-group">
            <span class="input-group-addon">Rp</span>
            <div class="form-line">
              <input type="text" name="maal" class="form-control only-num" placeholder="Jumlah Zakat Maal" value="{{$transaksi->zakat_maal}}">
            </div>
          </div>
          <div class="input-group">
            <span class="input-group-addon">Rp</span>
            <div class="form-line">
              <input type="text" name="infaq" class="form-control only-num" placeholder="Jumlah Infaq" value="{{$transaksi->infaq}}">
            </div>
          </div>
          <div class = "input-group">
            <span class       = "input-group-addon">Infaq Beras</span>
            <div class = "form-line">
                <input type  = "text" id= "beras_infaq" name = "beras_infaq" value="{{$transaksi->beras_infaq}}"
                    class = "form-control dec-num" placeholder = "Jumlah Beras Infaq">
            </div>
            <span class = "input-group-addon">Kg</span>
        </div>          
          <div class="row clearfix">
            <div class="">
              <input type="button" name="insert" id="insert" value="MASUKKAN" class="btn btn-primary m-t-15 waves-effect">
              <input type="reset" class="btn btn-primary m-t-15 waves-effect" value="RESET">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {

    function hitJumlahZakatBerat() {
        var beras = 2.5;
        var jiwa  = $("#jiwa").val();
        var res   = jiwa * beras;
        $("#beras").val(res);
    };

    $(".only-num").keypress(function(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
      return true;
    });



    $(".dec-num").keypress(function(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57)))
        return false;
      return true;
    });

    $("#jiwa").keyup(function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57)))
            return false;

        var jiwa = $("#jiwa").val();
        var nominal = $("#tipe option:selected").val();

        if (nominal == "1") {
            hitJumlahZakatBerat();
        } else {
            $("#beras").val("0");
            if (nominal == "0") {
                $("#uang").val("0");
            } else {
                $.ajax({
                    type: "GET",
                    url: "{{ url('nominal') }}/" + nominal + "",
                    success: function(data) {
                        var total = data.nominal * jiwa;
                        // total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
                        $("#uang").val(total);
                    }
                });
            }
        }
    });

    $("#beras").focus(function() {
      var jiwa = $("#jiwa").val();
        var beras = 2.5;

        var nominal = $("#tipe option:selected").val();
        var res = 0;

        if (nominal == "1") {
            res = jiwa * beras;
            $("#uang").val("0");
        }

        $("#beras").val(res);
    });

    $("#uang").focus(function() {
      var jiwa    = $("#jiwa").val();
      var nominal = $("#tipe option:selected").val();
      if (nominal == "0") {
        $("#uang").val("0");
      } else {
        $.ajax({
          type   : "GET",
          url    : "{{url('nominal')}}/" + nominal + "",
          success: function(data) {
            var total = data.nominal * jiwa;
            $("#uang").val(total);
          }
        });
      }
    });
    $("#insert").click(function() {
      swal({
        title             : 'Apakah Datanya Sudah Sesuai?',
        text              : "Pastikan Data Yang Anda Berikan Sesuai",
        type              : 'warning',
        showCancelButton  : true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Ya, Sudah Sesuai',
        cancelButtonText  : 'Tidak, batalkan!',
      }).then((result) => {
        if (result.value) {
          $('#myform').submit();
        }
      })
    });
  });
</script>
@endsection