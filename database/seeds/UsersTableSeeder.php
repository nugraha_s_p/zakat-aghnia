<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Webmaster',
            'username' => 'nugraha',
            'email' => 'nugraha@gmail.com',
            'password' => Hash::make('admin123'),
            'role_id' => '1',
            'status' => 'aktif'
        ]);
    }
}
