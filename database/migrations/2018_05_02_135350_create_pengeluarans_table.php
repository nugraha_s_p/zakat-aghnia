<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengeluaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengeluarans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jumlah')->nullable();
            $table->string('keterangan')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('jenismustahiq_id')->unsigned();
            $table->index('user_id');
            $table->index('jenismustahiq_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('jenismustahiq_id')->references('id')->on('jenis_mustahiqs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengeluarans');
    }
}
